package com.bakaeva.tm.constant;

import com.bakaeva.tm.endpoint.Role;
import com.bakaeva.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User USER3 = new User();

    @NotNull
    public final static User ADMIN1 = new User();

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER1, USER2, USER3, ADMIN1);

    static {
        for (int i = 0; i < USER_LIST.size(); i++) {
            @NotNull final User user = USER_LIST.get(i);
            user.setId("u-0" + i);
            user.setLogin("user" + i);
            user.setPasswordHash("hash" + i);
            user.setEmail("user" + i + "@us.er");
            user.setFirstName("Name" + i);
            user.setLastName("Last-Name" + i);
            user.setMiddleName("Middle-Name" + i);
            user.setRole(Role.USER);
        }
        ADMIN1.setRole(Role.ADMIN);
    }

}