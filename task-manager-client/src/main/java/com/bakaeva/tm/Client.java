package com.bakaeva.tm;

import com.bakaeva.tm.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;

public class Client {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}