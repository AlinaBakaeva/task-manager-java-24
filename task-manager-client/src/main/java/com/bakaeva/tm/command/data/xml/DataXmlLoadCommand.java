package com.bakaeva.tm.command.data.xml;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.endpoint.DomainEndpoint;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.bakaeva.tm.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA XML SAVE]");
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.saveToXml(session)) System.out.println("[OK]");
    }

}