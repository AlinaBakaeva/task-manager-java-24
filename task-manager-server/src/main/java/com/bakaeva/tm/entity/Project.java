package com.bakaeva.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String userId;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}