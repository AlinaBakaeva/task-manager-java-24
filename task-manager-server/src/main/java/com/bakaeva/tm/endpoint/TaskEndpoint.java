package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.api.IServiceLocator;
import com.bakaeva.tm.api.endpoint.ITaskEndpoint;
import com.bakaeva.tm.entity.Session;
import com.bakaeva.tm.entity.Task;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.empty.EmptyUserIdException;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTaskWithDescription(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void addTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().add(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void removeTaskAll(@WebParam(name = "session") @Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    public void removeTaskByUserAll(@WebParam(name = "session") @Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findTaskAll(@WebParam(name = "session") @Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        @Nullable final String userId = session.getUserId();
        return serviceLocator.getTaskService().findAll(userId);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        return serviceLocator.getTaskService().findById(userId, id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Task removeTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    @NotNull
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @NotNull
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

}