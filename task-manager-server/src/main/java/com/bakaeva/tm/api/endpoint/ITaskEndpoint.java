package com.bakaeva.tm.api.endpoint;

import com.bakaeva.tm.entity.Session;
import com.bakaeva.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    );

    @WebMethod
    void createTaskWithDescription(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @WebMethod
    void addTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "task") @Nullable Task task
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "task") @Nullable Task task
    );

    @WebMethod
    void removeTaskAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    void removeTaskByUserAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    List<Task> findTaskAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    Task findTaskById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    Task findTaskByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    Task findTaskByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    Task removeTaskById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    Task removeTaskByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    Task removeTaskByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    );

    @NotNull
    @WebMethod
    Task updateTaskById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @NotNull
    @WebMethod
    Task updateTaskByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "index") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

}
