package com.bakaeva.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void clear();

    void load(@Nullable List<E> list);

    @Nullable
    E remove(@Nullable E e);

}