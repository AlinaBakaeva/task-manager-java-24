package com.bakaeva.tm.api.endpoint;

import com.bakaeva.tm.entity.Project;
import com.bakaeva.tm.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void createProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    );

    @WebMethod
    void createProjectWithDescription(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @WebMethod
    void addProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "project") @Nullable Project project
    );

    @WebMethod
    void removeProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "project") @Nullable Project project
    );

    @WebMethod
    void removeProjectByUserAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    void removeProjectAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    List<Project> findProjectAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    Project findProjectById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    Project findProjectByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    Project findProjectByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    );

    @Nullable
    @WebMethod
    Project removeProjectById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    Project removeProjectByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    Project removeProjectByName(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    );

    @NotNull
    @WebMethod
    Project updateProjectById(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @NotNull
    @WebMethod
    Project updateProjectByIndex(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "index") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

}