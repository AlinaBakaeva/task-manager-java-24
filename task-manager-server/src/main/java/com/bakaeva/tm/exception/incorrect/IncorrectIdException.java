package com.bakaeva.tm.exception.incorrect;

public final class IncorrectIdException extends RuntimeException {

    public IncorrectIdException() {
        super("Error! Id is incorrect...");
    }

}