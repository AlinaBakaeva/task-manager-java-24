package com.bakaeva.tm.exception.empty;

public final class EmptyDescriptionException extends RuntimeException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}